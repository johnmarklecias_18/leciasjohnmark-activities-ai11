export const slides = [
    {
        id: '1',
        image: require('../images/image1.png'),
        background: require('../backgrounds/bg1.png'),
        title: 'Why I Chose Information Technology?',
        subtitle: 'I am a very curious person when it comes to technology. And I find "arranging something to systematicaly work" a fun, satisfying, and thrilling experience!',
    },
    {
        id: '2',
        image: require('../images/image2.png'),
        background: require('../backgrounds/bg2.png'),
        title: 'Turning Point',
        subtitle: 'One time I got too curious, that I decided to make a web app. My idea was to make an app using HTML and CSS even though I was not sure if it was possible.',
    },
    {
        id: '3',
        image: require('../images/image3.png'),
        background: require('../backgrounds/bg3.png'),
        title: 'Learning From Failures',
        subtitle: 'I thought I could make a simple pos and inventory app in just a few weeks, using my previous HTML and CSS knowledge, but instead I ended up learning several c# windows form applications.',
    },
    {
        id: '4',
        image: require('../images/image4.png'),
        background: require('../backgrounds/bg4.png'),
        title: 'Where It Took Off',
        subtitle: 'I eventually found my way to android development. Specifically kotlin, jetpack compose, a little bit of room database, and a decent understanding on MVVM.',
    },
    {
        id: '5',
        image: require('../images/image5.png'),
        background: require('../backgrounds/bg5.png'),
        title: 'My Inspiration',
        subtitle: 'Progress became evident after several months of self-learning. And the motivation for all of those efforts was to lessen my parents\' struggle in managing their growing business.',
    },
    {
        id: '6',
        image: require('../images/image6.png'),
        background: require('../backgrounds/bg6.png'),
        title: 'My Current State ',
        subtitle: 'It has already been a few weeks of learning React Native and I must say, this experience have been a lot easier than my previous. And I hope that I would learn more exciting stuff in this subject!',
    },
];