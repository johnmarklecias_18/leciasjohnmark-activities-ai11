import 'react-native-gesture-handler';
import React from 'react';
import {Image, StyleSheet, View, Text, TouchableOpacity, Dimensions,} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

// Data
import { slides } from './src/data/Slides';

const {width, height} = Dimensions.get('window');

const COLORS = {primary: '#282534', white: '#fff'};

const Slide = ({item}) => {
    
    return (
        <View 
            style={{
                alignItems: 'center',
                flex: 1,
                backgroundColor: COLORS.white, 
                justifyContent: 'flex-end',
                paddingTop: 50
            }}
        >
            <Image 
                source={item?.background} 
                style={{height: '100%', width: '200%', resizeMode: 'contain', opacity: 0.3}} 
            />

            <View style={{
                position: 'absolute', height: '100%', width: '100%', alignItems: 'center',}}>
                <Image
                    source={item?.image}
                    style={{height: '60%', width: '100%', resizeMode: 'contain',}}
                />
                <View>
                    <Text style={styles.title}>{item?.title}</Text>
                    <Text style={styles.subtitle}>{item?.subtitle}</Text>
                </View>
            </View>
        </View>

    );
};

export default function App() {

    const [currentSlideIndex, setCurrentSlideIndex] = React.useState(0);
    const ref = React.useRef();

    const updateCurrentSlideIndex = (index) => {
        setCurrentSlideIndex(index);
    };

    const nextSlide = () => {
        const nextSlideIndex = currentSlideIndex + 1;
        if (nextSlideIndex != slides.length) {
            ref?.current.navigate('Slide'+nextSlideIndex);
            setCurrentSlideIndex(nextSlideIndex);
        }
    };
    
    const skip = () => {
        const lastSlideIndex = slides.length - 1;
        ref?.current.navigate('Slide'+lastSlideIndex);
        setCurrentSlideIndex(lastSlideIndex);
    };

    const goToSlide = (index) => {
        ref?.current.navigate('Slide'+index);
        setCurrentSlideIndex(index);
    };

    const Pagination = () => {
        return (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
    
              {slides.map((_, index) => (
                <TouchableOpacity
                    key={index}
                    onPress={() => goToSlide(index)}
                >
                  <View
                    style={[
                        styles.indicator,
                        (
                          currentSlideIndex == index && {
                              backgroundColor: COLORS.primary,
                              width: 10,
                              height: 10,
                              borderRadius: 8
                          }
                        )
                    ]}
                  />
                </TouchableOpacity>
              ))}
    
            </View>
        )
    };

    const Footer =  () => {
        return (
            <View
                style={{
                height: height * 0.25,
                justifyContent: 'flex-start',
                alignItems: 'center',
                }}
            >
                <Pagination/>

                <View>
                    {
                        currentSlideIndex == slides.length - 1 ? (
                            <View style={{alignItems: 'center',}}>
                                <TouchableOpacity style={styles.button} onPress={() => goToSlide(0)}>
                                    <Text style={styles.buttonText}>Home</Text>
                                </TouchableOpacity>
                                <Text
                                    style={{
                                        color: 'grey', 
                                        fontSize:10 ,
                                        marginTop: 10,
                                        textAlign: 'center',
                                        maxWidth: width * 0.5
                                    }}
                                >
                                    Click this button to go back to the first screen
                                </Text>
                            </View>
                        ) : (
                            <View style={{alignItems: 'center',}}>
                                <TouchableOpacity style={styles.button} onPress={nextSlide}>
                                    <Text style={styles.buttonText}>Continue</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={styles.skipButton} onPress={skip}>
                                    <Text style={{fontSize: 15, color: 'grey',}}>Skip</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    }
                </View>
                
            </View>
        )
    }

    const Tab = createMaterialTopTabNavigator();

    return (
        <View style={{flex: 1,}}>
            <NavigationContainer
                ref={ref} 
                onStateChange={(state)=>updateCurrentSlideIndex(state.index)}
            >
                <Tab.Navigator
                    initialRouteName='Slide0' 
                    screenOptions={{headerShown: true, lazy: true,}}
                >
                    {slides.map((slide, index) => (
                        <Tab.Screen 
                            key={index}
                            name={"Slide"+index} 
                            options={{tabBarStyle: {display: 'none'}}}
                        >
                            {()=><Slide item={slide}/>}
                        </Tab.Screen>
                    ))}
                </Tab.Navigator>
            </NavigationContainer>

            <Footer/>
        </View>
        
    )
}

const styles = StyleSheet.create({
    subtitle: {
        color: COLORS.primary,
        fontSize: 14,
        marginTop: 10,
        maxWidth: width * 0.70,
        textAlign: 'center',
        lineHeight: 23,
        textShadowRadius: 20,
        textShadowColor: 'white',
    },
    title: {
        color: COLORS.primary,
        fontSize: 22,
        fontWeight: 'bold',
        marginTop: 20,
        textAlign: 'center',
        maxWidth: width * 0.75,
        textShadowRadius: 20,
        textShadowColor: 'white',
    },
    indicator: {
        height: 5,
        width: 5,
        backgroundColor: 'grey',
        margin: 5,
        borderRadius: 3,
    },
    button:{
        backgroundColor: '#1EAE94',
        width: width * 0.50,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        marginTop: 20,
    },
    buttonText:{
        color: COLORS.white,
        fontSize: 18,
    },
    skipButton: {
        marginTop: 10,
    },
});