import * as React from 'react';
import {useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button, Keyboard, TouchableOpacity, LayoutAnimation, UIManager, ScrollView} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

if (
    Platform.OS === "android" &&
    UIManager.setLayoutAnimationEnabledExperimental
) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
};

export default function HomeScreen() {

    const [todo, onSetTodo] = useState("");
    const [todos, onSetTodos] = useState([]);
    const [expanded, setExpanded] = useState(false);
  
    const onAddTodo = () => {
      if(todo !== "") {
        Keyboard.dismiss()
        onSetTodos([...todos, todo]);
        onSetTodo("");
        setExpanded(false);
      } else {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        setExpanded(!expanded);
      } 
    };
  
    const onCompleteTodo = (index) => {
      let itemsCopy = [...todos];
      itemsCopy.splice(index, 1);
      onSetTodos(itemsCopy);
    };
    

    // To-do item
    const Todo = ({todo, index}) => {
      return(
        <View style={styles.item}>
          <View style={styles.itemLeft}>
          <MaterialIcons name="drag-indicator" size={30} color='#3DBE29'/>
            <Text style={styles.itemText} numberOfLines={1}>{todo}</Text>
          </View>
          <TouchableOpacity onPress={() => onCompleteTodo(index)}>
            <Ionicons name="checkmark-circle" size={24} color='#3DBE29' />
          </TouchableOpacity>
        </View>
      )
    };
  

    // Main
    return(
      <View style={styles.container}>
      <View style={styles.todosWrapper}>
  
      <Text style={styles.screenTitle}>Here's your tasks</Text>
  
        <View style={styles.writeTodoWrapper}>
          <TextInput 
           onChangeText={onSetTodo}
           value={todo}
           style={styles.textInput}
           placeholder={"Enter what you want to do here"}
          />
          {expanded && (
            <Text style={styles.errorText}>Please enter something</Text>
          )}
          <Button 
           title={"Add to-do"}
           color={'#3DBE29'}
           onPress={onAddTodo}
          />
        </View>
  

        <ScrollView style={styles.todos}>
          {
            todos.map((item, index) => {
             return (
               <Todo todo={item} index={index} key={index}/>
             )
            }).reverse()
          }
        </ScrollView>
  
      </View>
      </View>
    )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    todosWrapper: {
      paddingTop: 50,
    },
    screenTitle: {
      fontSize: 26,
      fontWeight: 'bold',
      color: 'black',
      marginHorizontal: 20,
    },
    todos: {
      marginTop: 20,
      height: '77%',
      marginBottom: 20,
    },
    textInput: {
      height: 50,
      paddingHorizontal: 15,
      marginVertical: 10,
      backgroundColor: '#FFF',
      borderColor: '#3DBE29',
      elevation: 10,
      borderRadius: 10,
    },
    writeTodoWrapper: {
      elevation: 5,
      marginHorizontal: 20,
    },
    item:{
      backgroundColor: 'white',
      height: 60,
      padding: 16,
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
      elevation: 5,
      marginHorizontal: 20,
      marginVertical: 8,
    },
    itemLeft: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    itemText: {
      maxWidth: '80%',
      fontSize: 16,
      marginLeft: 16,
    },
    errorText: {
      color: 'red',
      marginBottom: 10,
      paddingHorizontal: 5,
    },
    completedTasks: {
      marginVertical: 10,
      paddingHorizontal: 5,
    },
});
