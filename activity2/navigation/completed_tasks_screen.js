import * as React from 'react';
import { StyleSheet, Text, View, } from 'react-native';

export default function CompletedTasksScreen() {

    return (
      <View style={styles.container}>
        <Text style={styles.screenTitle}>Completed Tasks</Text>
        <Text style={styles.description}numberOfLines={1}>Completed tasks go here</Text>
      </View>
    )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      paddingTop: 50,
    },
    screenTitle: {
      fontSize: 26,
      fontWeight: 'bold',
      color: 'black',
      marginHorizontal: 20,
    },
    description: {
      color: 'black',
      marginHorizontal: 20,
      marginTop: 20,
    },
});