import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';

//Screens
import add_task_screen from './navigation/add_task_screen.js';
import completed_tasks_screen from './navigation/completed_tasks_screen.js';

const Tab = createBottomTabNavigator();

export default function App() {

  return (
    <NavigationContainer>{
      <Tab.Navigator
        initialRouteName="Add Task"
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName
            let routeName = route.name

            if (routeName === "Add Task") {
              iconName = focused ? 'clipboard' : 'clipboard-outline'
              size = focused ? 24 : 20
              return <Ionicons name={iconName} size={size} color={color}/>
            }
            else if (routeName === "Completed Tasks") {
              iconName = 'tasks'
              size = focused ? 24 : 20
              return <FontAwesome5 name={iconName} size={size} color={color} />
            }
            
          },
          tabBarActiveTintColor: '#3DBE29',
          tabBarInactiveTintColor: 'black',
          tabBarLabelStyle: { paddingBottom:10, fontSize:10},
          tabBarStyle: {padding: 10, height: 60}
        })}
      >
        <Tab.Screen 
          name="Add Task" 
          component={add_task_screen} 
          options={{ headerShown: false }}
        />
        <Tab.Screen 
          name="Completed Tasks" 
          component={completed_tasks_screen}
          options={{ headerShown: false }}
        />
      </Tab.Navigator>
    }</NavigationContainer>
  );
}