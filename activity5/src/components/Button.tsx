import { StyleSheet, TouchableHighlight, Text, View, Dimensions} from "react-native";
import { myColors } from "../styles/Colors";

const {width} = Dimensions.get('window');
const size = width/4;

interface ButtonProps {
    onPress: () => void;
    title: string;
    color: string;
}

export default function Button({ title, color, onPress}: ButtonProps) {
    return (
        <TouchableHighlight onPress={onPress}>
            <View style={Styles.button}>
                <Text 
                    style={[{
                        fontSize: 32,
                        color: color,
                    }]}
                >
                    {title}
                </Text>
            </View>
        </TouchableHighlight>
    );
};

const Styles = StyleSheet.create({
    button: {
        width: size,
        height: size*0.70,
        backgroundColor: myColors.dark,
        justifyContent: "center",
        alignItems: "center",
    },
});