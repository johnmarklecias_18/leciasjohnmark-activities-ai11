import * as React from "react";
import Button from "./Button";
import { StyleSheet, View, Text, ScrollView, Dimensions } from "react-native";
import { myColors } from "../styles/Colors";

export const {width, height} = Dimensions.get('window');

export default function Calculator() {
  const ref = React.createRef<ScrollView>();

  const [input, setInput] = React.useState("");
  const [result, setResult] = React.useState("");
  
  const operations = ['+', '-', '×', '÷', '％', '.'];

  const onButtonPress = (value: string) => {
    if(
      operations.includes(value) && input === "" ||
      operations.includes(value) && operations.includes(input.slice(-1))
    ) {
      return;
    }

    if(result !== "" && operations.slice(0, -2).includes(value)) {
      setInput(result + value);
      setResult("");
    }
    else if(result !== "" && !operations.slice(0, -2).includes(value)) {
      setResult("");
      setInput("" + value);
    } 
    else {
      setInput(input + value);
    }
  };

  const clearInput = () => {
    setInput("");
    setResult("");
  };

  const deleteInput = () => {
    setInput(input.slice(0, -1));
  };

  const onEqualsPress = () => {
    if(!operations.includes(input.slice(-1)) && input !== "") {
      setResult(calculate);
    }
  };

  const onPercentPress = () => {
    if(!operations.includes(input.slice(-1)) && input !== "") {
      let x = calculate() + "*(0.01)";

      let res = new String(eval(x)).toString();

      if(res.split('.')[1]?.length > 6) {
        setResult(new String(eval(res).toFixed(5)).toString());
      }
      else {
        setResult(res);
      }
    }
  };  

  const calculate = (): any => {
    let fixedInput = input.split('×').join('*');
    fixedInput = fixedInput.split('÷').join('/');

    let res = new String(eval(fixedInput)).toString();

    if(res.split('.')[1]?.length > 6) {
      return new String(eval(res).toFixed(5)).toString();
    }
    else {
      return res;
    }
  };  

  const DisplayInput = () => {
    return (
      <View>
        <View style={Styles.displayInput}>
          <ScrollView 
            horizontal 
            ref={ref} 
            onContentSizeChange={() => ref.current?.scrollToEnd()}
          >
            <Text style={Styles.inputText}>
              {input}
            </Text>
          </ScrollView>
        </View>
        
        <View style={Styles.displayResult}>
          {
            result !== "" && (
              <Text style={Styles.resultText}>
                {result}
              </Text>
            )
          }
        </View>
      </View>
    )
  };

  return (
    <View>
      <DisplayInput/>

      <View>
        <View style={Styles.row}>
          <Button title="C" color={myColors.blue} onPress={clearInput} />
          <Button title="％" color={myColors.blue} onPress={onPercentPress} />
          <Button title="√" color={myColors.blue} onPress={() => onButtonPress("")} />
          <Button title="⌫" color={myColors.blue} onPress={deleteInput}/>
          
        </View>
        <View style={Styles.row}>
          <Button title="(" color={myColors.blue} onPress={() => onButtonPress("(")} />
          <Button title=")" color={myColors.blue} onPress={() => onButtonPress(")")} />
          <Button title="^" color={myColors.blue} onPress={() => onButtonPress("")} />
          <Button title="÷" color={myColors.blue} onPress={() => onButtonPress("÷")} />
        </View>
        <View style={Styles.row}>
          <Button title="7" color={myColors.light}  onPress={() => onButtonPress("7")} />
          <Button title="8" color={myColors.light} onPress={() => onButtonPress("8")} />
          <Button title="9" color={myColors.light} onPress={() => onButtonPress("9")} />
          <Button title="×" color={myColors.blue} onPress={() => onButtonPress("×")} />
        </View>
        <View style={Styles.row}>
          <Button title="4" color={myColors.light} onPress={() => onButtonPress("4")} />
          <Button title="5" color={myColors.light} onPress={() => onButtonPress("5")} />
          <Button title="6" color={myColors.light} onPress={() => onButtonPress("6")} />
          <Button title="-" color={myColors.blue} onPress={() => onButtonPress("-")} />
        </View>
        <View style={Styles.row}>
          <Button title="1" color={myColors.light} onPress={() => onButtonPress("1")} />
          <Button title="2" color={myColors.light} onPress={() => onButtonPress("2")} />
          <Button title="3" color={myColors.light} onPress={() => onButtonPress("3")} />
          <Button title="+" color={myColors.blue}  onPress={() => onButtonPress("+")} />
        </View>
        <View style={Styles.row}>
          <Button title="00" color={myColors.light} onPress={() => onButtonPress("")} />
          <Button title="0" color={myColors.light} onPress={() => onButtonPress("0")} />
          <Button title="." color={myColors.light} onPress={() => onButtonPress(".")} />
          <Button title="=" color={myColors.blue} onPress={onEqualsPress} />
        </View>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  // Keyboard
  row: {
      flexDirection: "row",
  },
  displayInput: {
      alignItems: "flex-end",
      margin: 10,
  },
  displayResult: {
      maxWidth: width,
      alignItems: "flex-end",
      margin: 10,
  },
  inputText: {
      fontSize: 40,
      color: myColors.light,
  },
  resultText: {
      fontSize: 60,
      color: myColors.light
  },
})