export const myColors = {
    light: '#F1F2F3',
    dark: '#17171C',
    blue: '#4B5EFC',
    gray: '#4E505F',
    darkGray: '#2E2F38',
    black: '#000000',
    white: '#FFFFFF', 
    result: '#46D5B2',
}