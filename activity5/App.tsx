
import { StyleSheet, Switch, View } from 'react-native';
import { myColors } from './src/styles/Colors';
import Calculator from './src/components/Calculator';

export default function App() {
  return (
    <View style={styles.container}>
      <Calculator/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: myColors.gray,
    justifyContent: "flex-end",
  },
});
