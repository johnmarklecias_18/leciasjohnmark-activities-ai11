import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// Contexts
import ItemProvider from './src/context/ItemProvider';
import HistoryProvider from './src/context/HistoryProvider';
import ItemHistoryProvider from './src/context/ItemHistoryProvider';
import CategoryProvider from './src/context/CategoryProvider';
import BrandProvider from './src/context/BrandProvider';
import ItemsFilterProvider from './src/context/ItemsFilterProvider';

//Screens
import MainNavigation from './src/screens/MainNavigation';
import AddEditItemScreen from './src/screens/AddEditItemScreen';
import UpdateStockScreen from './src/screens/UpdateStockScreen';

const Stack = createNativeStackNavigator();

export default function App() {

  return (
    <NavigationContainer>{
      <ItemProvider>
        <HistoryProvider>
          <ItemHistoryProvider>
            <CategoryProvider>
              <BrandProvider>
                <ItemsFilterProvider>
                  <Stack.Navigator initialRouteName='Main Navigation'>
                    <Stack.Screen name="Main Navigation" component={MainNavigation} options={{ headerShown: false }}/>
                    <Stack.Screen name="Add Edit Item" component={AddEditItemScreen} options={{ headerShown: false }}/>
                    <Stack.Screen name="Update Stock" component={UpdateStockScreen} options={{ headerShown: false }}/>
                  </Stack.Navigator>
                </ItemsFilterProvider>
              </BrandProvider>
            </CategoryProvider>
          </ItemHistoryProvider>
        </HistoryProvider>
      </ItemProvider>
    }</NavigationContainer>
  );
}