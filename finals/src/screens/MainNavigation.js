import * as React from 'react';
import { StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// Constants
import COLORS from '../constants/colors.js';

// Components
import FloatingButton from '../components/FloatingButton.js';

// Screens
import ItemsListScreen from './ItemsListScreen';
import StockHistoryScreen from './StockHistoryScreen';

const Tab = createBottomTabNavigator();

export default function MainNavigation({navigation}) {

  return (
    <>
      <Tab.Navigator
        initialRouteName="Items"
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName
            let routeName = route.name

            if (routeName === "Items") {
              iconName = focused ? 'cart' : 'cart-outline'
              size = focused ? 24 : 20
              return <Icon name={iconName} style={{color: color, fontSize: 22}}/>
            }
            else if (routeName === "History") {
              iconName = focused ? 'archive-clock' : 'archive-clock-outline'
              size = focused ? 24 : 20
              return <Icon name={iconName} style={{color: color, fontSize: 22}}/>
            }
            
          },
          tabBarActiveTintColor: COLORS.green,
          tabBarInactiveTintColor: COLORS.black,
          tabBarLabelStyle: { paddingBottom:10, fontSize:10},
          tabBarStyle: {padding: 10, height: 60}
        })}
      >
        <Tab.Screen 
          name="Items" 
          component={ItemsListScreen} 
          options={{ headerShown: false }}
        />
        <Tab.Screen 
          name="History" 
          component={StockHistoryScreen}
          options={{ headerShown: false }}
        />
      </Tab.Navigator>
      <FloatingButton 
      iconName={"archive-sync"} 
      onPress={() => {navigation.navigate("Update Stock")}}
      style={styles.floatingBtn}
      />
    </>
  );
};

const styles = StyleSheet.create({
  floatingBtn: {
      position: 'absolute',
      right: 15,
      bottom: 70,
      zIndex: 1,
  },
});