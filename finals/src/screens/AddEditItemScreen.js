import AsyncStorage from '@react-native-async-storage/async-storage';
import * as React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Keyboard,
  ScrollView,
  Alert,
  Dimensions
} from 'react-native';

// Constants
import COLORS from '../constants/colors.js';

// Components
import Button from '../components/Button.js';
import Input from '../components/Input';
import Loader from '../components/Loader';

// Data
import { useItems } from '../context/ItemProvider.js';
import { useHistory } from '../context/HistoryProvider.js';
import { useItemHistory } from '../context/ItemHistoryProvider.js';
import { useBrands } from '../context/BrandProvider.js';
import { useCategories } from '../context/CategoryProvider.js';

const {width, height} = Dimensions.get('window');

export default function AddEditItemScreen({navigation}) {

  const [item, setItem] = React.useState({
    name: '',
    sku: '',
    stock: '',
    reorderPoint: '',
    category: '',
    brand: '',
    price: '',
    cost: ''
  });
  const [errors, setErrors] = React.useState({});
  const [loading, setLoading] = React.useState(false);

  const {items, setItems} = useItems();
  const {history, setHistory} = useHistory();
  const {itemHistory, setItemHistory} = useItemHistory();
  const {brands, setBrands} = useBrands();
  const {categories, setCategories} = useCategories();

  const validate = () => {
    Keyboard.dismiss();

    let isValid = true;
    let isBrandUnique = true;
    let isCategoryUnique = true;

    if (!item.name) {
      onError('Please enter an item name', 'name');
      isValid = false;
    } 

    if (!item.sku) {
      onError('Please enter an sku', 'sku');
      isValid = false;
    } 
    else {
      const thisSku = item.sku
      items.forEach((item) => {
        if(item.sku === thisSku) {
          onError('Sku already exists', 'sku');
          isValid = false;
        }
      });
    }

    if (!item.reorderPoint) {
      onError('Please enter a reorder point', 'reorderPoint');
      isValid = false;
    }

    if (!item.stock) {
      onError('Please enter a number', 'stock');
      isValid = false;
    }

    if (!item.category) {
      onError('Please enter a category', 'category');
      isValid = false;
    } else {
      categories.forEach((category) => {
        if(category.name === item.category) {
          isCategoryUnique = false;
        }
      });
    }

    if (!item.brand) {
      onError('Please enter a brand', 'brand');
      isValid = false;
    } else {
      brands.forEach((brand) => {
        if(brand.name === item.brand) {
          isBrandUnique = false;
        }
      });
    }

    if (!item.price) {
      onError('Please enter a price', 'price');
      isValid = false;
    }

    if (!item.cost) {
      onError('Please enter the cost', 'cost');
      isValid = false;
    }

    if (isBrandUnique && isValid) {
      addBrand();
    }

    if (isCategoryUnique && isValid) {
      addCategory();
    }

    if (isValid) {
      addItem();
    }
  };

  const addItem = () => {
    const date = Date.now();
    setLoading(true);

    setTimeout(() => {
      try {
        setLoading(false);

        const newHistory = {
          date: date,
          items: [item.sku],
          names: [item.name],
          stockFlow: [parseInt(item.stock)]
        };

        const newItemHistory = {
          date: date,
          itemSku: item.sku,
          stockFlow: parseInt(item.stock),
        };

        const updatedItems = [...items, item];
        setItems(updatedItems);
        const updatedHistory = [...history, newHistory];
        setHistory(updatedHistory);
        const updatedItemHistory = [...itemHistory, newItemHistory];
        setItemHistory(updatedItemHistory);

        const itemPair = ["@items", JSON.stringify(updatedItems)];
        const historyPair = ["@history", JSON.stringify(updatedHistory)];
        const itemHistoryPair = ["@itemhistory", JSON.stringify(updatedItemHistory)]

        AsyncStorage.multiSet([itemPair, historyPair, itemHistoryPair]);

        setItem({});
        navigation.navigate('Items');
      } catch (error) {
        Alert.alert('Error', 'Failed to add item');
      }
    }, 100);
  };

  const addBrand = () => {
    try{
      const newBrand = {
        name: item.brand
      };
  
      const updatedBrands = [...brands, newBrand];
      setBrands(updatedBrands);
  
      AsyncStorage.setItem("@brands", JSON.stringify(updatedBrands));
    } catch (error) {
      Alert.alert('Error', 'Failed to add brand');
    }
  };

  const addCategory = () => {
    try{
      const newCategory = {
        name: item.category
      };
  
      const updatedCategories = [...categories, newCategory];
      setCategories(updatedCategories);
  
      AsyncStorage.setItem("@categories", JSON.stringify(updatedCategories));
    } catch (error) {
      Alert.alert('Error', 'Failed to add category');
    }
  };

  const onTextChange = (text, input) => {
    setItem(prevState => ({...prevState, [input]: text}));
  };

  const onError = (error, input) => {
    setErrors(prevState => ({...prevState, [input]: error}));
  };

  return (
    <SafeAreaView style={{backgroundColor: COLORS.white, flex: 1}}>
      <Loader visible={loading} />
      <ScrollView
        contentContainerStyle={{paddingTop: 20, paddingHorizontal: 20}}
        showsVerticalScrollIndicator={false}
      >
        <Text style={{color: COLORS.black, fontSize: 20, fontWeight: 'bold'}}>
          Add Item
        </Text>
        <View style={{marginVertical: 20}}>
          <Input
            value={item.name}
            onChangeText={text => onTextChange(text, 'name')}
            onFocus={() => onError(null, 'name')}
            label="Item Name"
            placeholder="Enter item name"
            error={errors.name}
          />
          <Input
            value={item.sku}
            onChangeText={text => onTextChange(text.replace(/ /g, ''), 'sku')}
            onFocus={() => onError(null, 'sku')}
            label="SKU"
            placeholder="Enter SKU"
            autoCapitalize="none"
            error={errors.sku}
          />
          <View style={{flexDirection: "row", alignItems: "baseline", justifyContent: "space-between"}}>
            <Input
              value={item.stock}
              keyboardType="numeric"
              onChangeText={text => onTextChange(text, 'stock')}
              onFocus={() => onError(null, 'stock')}
              label="In Stock Value"
              placeholder="0.0"
              error={errors.stock}
              width={width/2 * 0.87}
            />
            <Input
              value={item.reorderPoint}
              keyboardType="numeric"
              onChangeText={text => onTextChange(text, 'reorderPoint')}
              onFocus={() => onError(null, 'reorderPoint')}
              label="Reorder Point"
              placeholder="0.0"
              error={errors.reorderPoint}
              width={width/2 * 0.87}
            />
          </View>
          <Input
            value={item.category}
            onPickerPress={(item) => onTextChange(item, 'category')}
            items={categories}
            onChangeText={text => onTextChange(text, 'category')}
            onFocus={() => onError(null, 'category')}
            label="Category"
            placeholder="Select Category"
            error={errors.category}
            iconName={'chevron-down'}
          />
          <Input
            value={item.brand}
            onPickerPress={(item) => onTextChange(item, 'brand')}
            items={brands}
            onChangeText={text => onTextChange(text, 'brand')}
            onFocus={() => onError(null, 'brand')}
            label="Brand"
            placeholder="Select Brand"
            error={errors.brand}
            iconName={'chevron-down'}
          />
          <View style={{flexDirection: "row", alignItems: 'flex-start', justifyContent: "space-between"}}>
            <Input
              value={item.price}
              keyboardType="numeric"
              onChangeText={text => onTextChange(text, 'price')}
              onFocus={() => onError(null, 'price')}
              label="Price per Item (PHP)"
              placeholder="00.00"
              error={errors.price}
              width={width/2 * 0.87}
            />
            <Input
              value={item.cost}
              keyboardType="numeric"
              onChangeText={text => onTextChange(text, 'cost')}
              onFocus={() => onError(null, 'cost')}
              label="Cost of Stock (PHP)"
              placeholder="00.00"
              error={errors.cost}
              width={width/2 * 0.87}
            />
          </View>
          <Button title="ADD ITEM" onPress={validate} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};