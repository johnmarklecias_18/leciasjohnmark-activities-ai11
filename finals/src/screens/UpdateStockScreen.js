import 'react-native-gesture-handler';
import * as React from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Dimensions, 
  SafeAreaView,
  StatusBar, 
  FlatList,
  TouchableOpacity,
  Modal,
  Alert,
  SectionList,
  TextInput,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// Constants
import COLORS from '../constants/colors';

// Components
import SearchBar from '../components/SearchBar';
import UpdateStockItem from '../components/UpdateStockItem';
import RadioButton from '../components/RadioButton';


// Data
import { useItems } from '../context/ItemProvider';
import { useBrands } from '../context/BrandProvider.js';
import { useCategories } from '../context/CategoryProvider.js';
import { useItemsFilter } from '../context/ItemsFilterProvider';

const {width, height} = Dimensions.get('window');

const sortData = (data, filter) => {
  return data.sort((a, b) => {
    if (filter.ascending) return (a.name > b.name) ? 1 : -1
    else return (a.name > b.name) ? -1 : 1} );
};

export default function UpdateStockScreen({navigation}) {
  const [searchQuery, setSearchQuery] = React.useState('');
  const [displayItems, setDisplayItems] = React.useState([]);
  const [selectedItems, setSelectedItems] = React.useState([]);

  const [isFilterVisible, setIsFilterVisible] = React.useState(false);
  const [isSelectVisible, setIsSelectVisible] = React.useState(false);
  
  const {items} = useItems();
  const {brands} = useBrands();
  const {categories} = useCategories();
  const {filter, setFilter, filterItems} = useItemsFilter();

  const BRAND_SECTIONS = sortData(brands, filter).map((brand) => {
    let brandData = filterItems(items).filter((item) => {
      if(item.brand) {
        if (item.brand === brand.name) {
          return item;
        }
      }
    });
    return {title: brand.name, data: brandData};
  });

  const CATEGORY_SECTIONS = sortData(categories, filter).map((category) => {
    let categoryData = filterItems(items).filter((item) => {
      if(item.category) {
        if (item.category === category.name) {
          return item;
        }
      }
    });
    return {title: category.name, data: categoryData};
  });

  React.useEffect(() => {
    setDisplayItems(filterItems(items));
  }, [items, filter]);

  const onSearchInput = (text) => {
    setSearchQuery(text);

    if(text) {
      const filteredItems = items.filter((item) => {
        if(item.name) {
          if (item.name.toLowerCase().includes(text.toLowerCase())) {
            return item;
          }
        }
      });

      if (filteredItems.length) {
        setDisplayItems([...filteredItems]);
      } else {
        setDisplayItems([])
      }
    }
    else {
      setDisplayItems(items);
    }
  };

  const onClearSearch = () => {
    setSearchQuery('');
    setDisplayItems(items);
  };

  const onFilterPress = (value, filter) => {
    setFilter(prevState => ({...prevState, [filter]: value}));
  };

  const onSelectItem = (newSelectedItem) => {
    let selectedItemsCopy = [...selectedItems];

    selectedItemsCopy.push(newSelectedItem);
    const filteredSelectedItems = selectedItemsCopy.filter((selectedItem) => selectedItem.sku !== newSelectedItem.sku);

    if(filteredSelectedItems.length) {
      selectedItemsCopy = [...filteredSelectedItems, newSelectedItem]
    } else {
      selectedItemsCopy = [...filteredSelectedItems, newSelectedItem]
    }
    
    setSelectedItems([]);
    setSelectedItems([...selectedItemsCopy]);
  }

  const onDeleteSelectedItem = (item) => {
    let selectedItemsCopy = [...selectedItems];

    const filteredSelectedItems = selectedItemsCopy.filter((selectedItem) => selectedItem.sku !== item.sku);

    if(filteredSelectedItems.length) {
      selectedItemsCopy = [...filteredSelectedItems]
    } else {
      selectedItemsCopy = [...filteredSelectedItems]
    }
    
    setSelectedItems([]);
    setSelectedItems([...selectedItemsCopy]);
  }

  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor={COLORS.lightgreen}/>
        <SafeAreaView>
          <View style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
            <View style={{flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
              <Icon
                onPress={() => navigation.navigate("Main Navigation")}
                name={"arrow-left"} 
                color={COLORS.black} 
                size={25}
                style={{marginTop: 10, marginLeft: 15}}
              />
              <Text style={styles.screenTitle}>Update Stock</Text>
            </View>
            <TouchableOpacity
              onPress={() => navigation.navigate('Main Navigation')}
            >
              <Text style={{fontWeight: "bold", marginTop: 10, marginHorizontal: 20, color: COLORS.black,}}>DONE</Text>
            </TouchableOpacity>
          </View>
            
          <View style={styles.descriptionBox}>
            <Text style={styles.boxText}>Description</Text>
            <TextInput
              onChangeText={(text) => {}}
              placeholderTextColor={COLORS.grey}
              color={COLORS.black}
              style={{borderBottomWidth: 1, borderBottomColor: COLORS.black, marginHorizontal: 15, marginBottom: 15, height: 40}}
              maxLength={50}
            />
          </View>

          <TouchableOpacity 
            style={styles.selectItems} 
            activeOpacity={1}
            onPress={() => setIsSelectVisible(true)}
          >
            {
              (!selectedItems.length) ? (
                <View style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                  <Text style={[styles.boxText, {marginVertical: 15, marginLeft: 20}]}>Select Items</Text>
                  <Icon
                    name={"chevron-right"}
                    color={COLORS.black}
                    size={25}
                    style={{marginHorizontal: 20}}
                  />
                </View>
              ) : (
                <View style={{maxHeight: height*0.7, alignItems: "flex-start", justifyContent: "flex-start"}}>
                  <View style={{flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
                    <Icon
                      name={"plus-circle"}
                      color={COLORS.green}
                      size={25}
                      style={{marginLeft: 20}}
                    />
                    <Text style={[styles.boxText, {marginVertical: 15}]}>Add more items</Text>
                  </View>
                  <FlatList
                    data={selectedItems}
                    renderItem={({item}) => {
                      return (
                        <View 
                          style={{
                            marginHorizontal: 20, 
                            marginBottom: 10, 
                            backgroundColor: COLORS.light,
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            width: width*0.82
                          }}
                        >
                          <View>
                            <Text 
                              style={{
                                fontSize: 20,
                                fontWeight: 'bold',
                                color: COLORS.black,
                                marginLeft: 20,
                                marginTop: 10,
                                textAlign: 'left',}
                              }
                            >{item.name}</Text>
                            <Text 
                              style={{
                                fontSize: 15,
                                marginLeft: 20,
                                marginBottom: 10,
                                color: COLORS.black,
                                textAlign: 'left',}
                              }
                            >{item.prevStockValue}{" + ("}{item.stockFlow}{")"}</Text>
                          </View>
                          <Icon
                            onPress={() => onDeleteSelectedItem(item)}
                            name={"close"} 
                            size={30} 
                            style={{color: COLORS.black, margin: 20}}
                          />
                        </View>
                      )
                    }}
                  />
                </View>
              )
            }
          </TouchableOpacity>
        </SafeAreaView>
      
      <Modal visible={isSelectVisible} animationType="slide"> 
        <SafeAreaView style={styles.container}>
          <View style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
            <View style={{flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
              <Icon
                onPress={() => setIsSelectVisible(false)}
                name={"arrow-left"} 
                color={COLORS.black} 
                size={25}
                style={{marginTop: 10, marginLeft: 15}}
              />
              <Text style={styles.screenTitle}>Select Items</Text>
            </View>
            
            <TouchableOpacity
              onPress={() => setIsSelectVisible(false)}
            >
              <Text style={{fontWeight: "bold", marginTop: 10, marginHorizontal: 20, color: COLORS.black,}}>SAVE</Text>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
            <SearchBar
              value={searchQuery}
              onChangeText={(text) => onSearchInput(text)}
              containerStyle={{ margin: 10, width: width*0.8 }}
              onClear={() => onClearSearch()}
            />
            <>
              <TouchableOpacity
                onPress={() => setIsFilterVisible(true)}
              >
                <Icon
                  name='filter-variant'
                  size={30}
                  style={{color: COLORS.black}}
                />
              </TouchableOpacity>
              <Modal visible={isFilterVisible} animationType="slide" transparent={true}>
                <View style={styles.centeredView}>
                  <View style={styles.modalView}>
                    <Text style={styles.title} >Filter</Text>
                    
                    <View style={{flexDirection: "row", alignSelf: "flex-start", alignItems: "center", justifyContent: "space-around", marginHorizontal: 10}}>
                      <RadioButton 
                        label={"Name"} 
                        selected={filter.orderType === "name"} 
                        onPress={() => onFilterPress("name", "orderType")} 
                      />
                      <RadioButton 
                        label={"Category"} 
                        selected={filter.orderType === "category"}
                        onPress={() => onFilterPress("category", "orderType")}
                      />
                      <RadioButton 
                        label={"Brand"} 
                        selected={filter.orderType === "brand"} 
                        onPress={() => onFilterPress("brand", "orderType")}
                      />
                    </View>

                    <View style={{flexDirection: "row", alignSelf: "flex-start", alignItems: "center", justifyContent: "space-around", marginHorizontal: 10}}>
                      <RadioButton 
                        label={"Ascending"} 
                        selected={filter.ascending}
                        onPress={() => onFilterPress(true, "ascending")}
                      />
                      <RadioButton 
                        label={"Descending"} 
                        selected={!filter.ascending} 
                        onPress={() => onFilterPress(false, "ascending")}
                      />
                    </View>

                    <TouchableOpacity onPress={() => setIsFilterVisible(false)} style={{flexDirection: "column",alignItems: "center"}}>
                      <Text style={[styles.subtitle, {fontWeight: "bold"}]}>Done</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
            </>
          </View>

          {
            (filter.orderType === "name")? (
              <FlatList
                data={displayItems}
                renderItem={({ item }) => (
                  <UpdateStockItem
                    item={item}  
                    key={item.sku}
                    selected={selectedItems.find((selectedItem) => {return selectedItem.sku === item.sku})}
                    onSelect={(newSelectedItem) => onSelectItem(newSelectedItem)}
                  />
                )}
              />
            ) : (
              <SectionList 
                sections={(filter.orderType === "brand")? BRAND_SECTIONS : CATEGORY_SECTIONS}
                renderItem={({item}) => (
                  <UpdateStockItem
                    item={item}  
                    key={item.sku}
                    selected={selectedItems.find((selectedItem) => {return selectedItem.sku === item.sku})}
                    onSelect={(newSelectedItem) => onSelectItem(newSelectedItem)}
                  />
                )}
                renderSectionHeader={({section}) => {
                  return <Text style={[
                    styles.title, 
                    {marginVertical: 5, fontSize: 15, textTransform: "uppercase"}
                  ]}>{section.title}</Text>
                }}
              />
            )
          }
        </SafeAreaView>
      </Modal>
    </>
  )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: COLORS.light,
    },
    screenTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      color: 'black',
      marginTop: 10,
      marginHorizontal: 15
    },
    description: {
      color: 'black',
      marginHorizontal: 20,
      marginTop: 20,
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    modalView: {
      width: width*0.9,
      maxHeight: height*0.60,
      backgroundColor: COLORS.white,
      borderRadius: 20,
      elevation: 25
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      color: COLORS.black,
      marginHorizontal: 20,
      marginVertical: 15,
      textAlign: 'left',
    },
    subtitle: {
      fontSize: 15,
      color: COLORS.black,
      margin: 20,
      textAlign: 'center',
    },
    text: {
      fontSize: 15,
      color: COLORS.black,
      margin: 10,
      textAlign: 'center',
    },
    descriptionBox: {
      maxHeight: height*0.4,
      backgroundColor: COLORS.white, 
      marginBottom: 5,
      marginHorizontal: 10,
      borderRadius: 15,
      borderWidth: 0.5,
      borderColor: COLORS.grey,
      marginVertical: 20,
    },
    selectItems: {
      maxHeight: height*0.7,
      backgroundColor: COLORS.white, 
      marginBottom: 5,
      marginHorizontal: 10,
      borderRadius: 15,
      borderWidth: 0.5,
      borderColor: COLORS.grey,
      marginVertical: 20,
    },
    boxText:{
      color: COLORS.black, 
      textAlign: "left", 
      fontWeight: "500", 
      fontSize: 15, 
      marginHorizontal: 15, 
      marginTop: 15
    },
});