import 'react-native-gesture-handler';
import * as React from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Dimensions, 
  SafeAreaView,
  StatusBar, 
  FlatList,
  TouchableOpacity,
  Modal,
  Alert,
  SectionList,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// Constants
import COLORS from '../constants/colors';

// Components
import SearchBar from '../components/SearchBar';
import Item from '../components/Item';
import RadioButton from '../components/RadioButton';

// Data
import { useItems } from '../context/ItemProvider';
import { useBrands } from '../context/BrandProvider.js';
import { useCategories } from '../context/CategoryProvider.js';
import { useItemsFilter } from '../context/ItemsFilterProvider';

const {width, height} = Dimensions.get('window');

const sortData = (data, filter) => {
  return data.sort((a, b) => {
    if (filter.ascending) return (a.name > b.name) ? 1 : -1
    else return (a.name > b.name) ? -1 : 1} );
};

export default function ItemsListScreen({navigation}) {
  const [searchQuery, setSearchQuery] = React.useState('');
  const [displayItems, setDisplayItems] = React.useState([]);
  const [isFilterVisible, setIsFilterVisible] = React.useState(false);
  
  const {items} = useItems();
  const {brands} = useBrands();
  const {categories} = useCategories();
  const {filter, setFilter, filterItems} = useItemsFilter();

  const BRAND_SECTIONS = sortData(brands, filter).map((brand) => {
    let brandData = filterItems(items).filter((item) => {
      if(item.brand) {
        if (item.brand === brand.name) {
          return item;
        }
      }
    });
    return {title: brand.name, data: brandData};
  });

  const CATEGORY_SECTIONS = sortData(categories, filter).map((category) => {
    let categoryData = filterItems(items).filter((item) => {
      if(item.category) {
        if (item.category === category.name) {
          return item;
        }
      }
    });
    return {title: category.name, data: categoryData};
  });

  React.useEffect(() => {
    setDisplayItems(filterItems(items));
  }, [items, filter]);

  const onSearchInput = (text) => {
    setSearchQuery(text);

    if(text) {
      const filteredItems = items.filter((item) => {
        if(item.name) {
          if (item.name.toLowerCase().includes(text.toLowerCase())) {
            return item;
          }
        }
      });

      if (filteredItems.length) {
        setDisplayItems([...filteredItems]);
      } else {
        setDisplayItems([])
      }
    }
    else {
      setDisplayItems(items);
    }
  };

  const onClearSearch = () => {
    setSearchQuery('');
    setDisplayItems(filterItems(items));
  };

  const onFilterPress = (value, filter) => {
    setFilter(prevState => ({...prevState, [filter]: value}));
  };

  const onDoneFilterPress = () => {
    setIsFilterVisible(false);
    try{
      AsyncStorage.setItem("@itemsfilter", JSON.stringify(filter));
    } catch (error) {
      Alert.alert('Error', 'Failed to add items filter \n' + error);
    }
  };

  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor={COLORS.lightgreen}/>
      <SafeAreaView style={styles.container}>
        <View style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
          <Text style={styles.screenTitle}>Your Items</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate("Add Edit Item")}
          >
            <Icon
              name='plus'
              size={30}
              style={{color: COLORS.black, marginTop: 10, marginHorizontal: 20 }}
            />
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: "row", alignItems: "center", justifyContent: "flex-start"}}>
          <SearchBar
            value={searchQuery}
            onChangeText={(text) => onSearchInput(text)}
            containerStyle={{ margin: 10, width: width*0.8 }}
            onClear={() => onClearSearch()}
          />
          <>
            <TouchableOpacity
              onPress={() => setIsFilterVisible(true)}
            >
              <Icon
                name='filter-variant'
                size={30}
                style={{color: COLORS.black}}
              />
            </TouchableOpacity>
            <Modal visible={isFilterVisible} animationType="slide" transparent={true}>
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.title} >Filter</Text>
                  
                  <View style={{flexDirection: "row", alignSelf: "flex-start", alignItems: "center", justifyContent: "space-around", marginHorizontal: 10}}>
                    <RadioButton 
                      label={"Name"} 
                      selected={filter.orderType === "name"} 
                      onPress={() => onFilterPress("name", "orderType")} 
                    />
                    <RadioButton 
                      label={"Category"} 
                      selected={filter.orderType === "category"}
                      onPress={() => onFilterPress("category", "orderType")}
                    />
                    <RadioButton 
                      label={"Brand"} 
                      selected={filter.orderType === "brand"} 
                      onPress={() => onFilterPress("brand", "orderType")}
                    />
                  </View>

                  <View style={{flexDirection: "row", alignSelf: "flex-start", alignItems: "center", justifyContent: "space-around", marginHorizontal: 10}}>
                    <RadioButton 
                      label={"Ascending"} 
                      selected={filter.ascending}
                      onPress={() => onFilterPress(true, "ascending")}
                    />
                    <RadioButton 
                      label={"Descending"} 
                      selected={!filter.ascending} 
                      onPress={() => onFilterPress(false, "ascending")}
                    />
                  </View>

                  <TouchableOpacity onPress={() => onDoneFilterPress()} style={{flexDirection: "column",alignItems: "center"}}>
                    <Text style={[styles.subtitle, {fontWeight: "bold", color: COLORS.green,}]}>DONE</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </>
        </View>

        {
          (filter.orderType === "name")? (
            <FlatList
              data={displayItems}
              renderItem={({ item, index }) => (
                <Item item={item} key={index}/>
              )}
            />
          ) : (
            <SectionList 
              sections={(filter.orderType === "brand")? BRAND_SECTIONS : CATEGORY_SECTIONS}
              renderItem={({item}) => (<Item item={item} key={item.sku}/>)}
              renderSectionHeader={({section}) => {
                return <Text style={[
                  styles.title, 
                  {marginVertical: 5, fontSize: 15, textTransform: "uppercase", fontWeight: "500",}
                ]}>{section.title}</Text>
              }}
            />
          )
        }
      </SafeAreaView>
    </>
  )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: COLORS.light,
    },
    screenTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      color: 'black',
      marginTop: 10,
      marginHorizontal: 20
    },
    description: {
      color: 'black',
      marginHorizontal: 20,
      marginTop: 20,
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    modalView: {
      width: width*0.9,
      maxHeight: height*0.60,
      backgroundColor: COLORS.white,
      borderRadius: 20,
      elevation: 25
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      color: COLORS.black,
      marginHorizontal: 20,
      marginVertical: 15,
      textAlign: 'left',
    },
    subtitle: {
      fontSize: 15,
      color: COLORS.black,
      margin: 20,
      textAlign: 'center',
    },
    text: {
      fontSize: 15,
      color: COLORS.black,
      margin: 10,
      textAlign: 'center',
    },
});