import * as React from 'react';
import { StyleSheet, Text, View, SafeAreaView, StatusBar, FlatList, Dimensions} from 'react-native';

// Constants
import COLORS from '../constants/colors';

// Components
import HistoryItem from '../components/HistoryItem';

// Data
import { useHistory } from '../context/HistoryProvider';

const {width, height} = Dimensions.get('window');

const reverseData = data => {
  return data.sort((a, b) => {
    const aInt = parseInt(a.date);
    const bInt = parseInt(b.date);
    if (aInt < bInt) return 1;
    if (aInt == bInt) return 0;
    if (aInt > bInt) return -1;
  });
};

export default function StockHistoryScreen() {
  const {history } = useHistory();
  const reverseHistory = reverseData(history);

  return (
    <>
    <StatusBar barStyle='dark-content' backgroundColor={COLORS.lightgreen}/>
    <SafeAreaView style={styles.container}>
      <Text style={styles.screenTitle}>Stock History</Text>

      <FlatList
        data={reverseHistory}
        renderItem={({ item, index }) => (
          <HistoryItem item={item} key={index}/>
        )}
      />
    </SafeAreaView>
  </>
  )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: COLORS.light,
    },
    screenTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      color: 'black',
      marginTop: 10,
      marginBottom: 20,
      marginHorizontal: 20
    },
    description: {
      color: 'black',
      marginHorizontal: 20,
      marginTop: 20,
    },
});