export default COLORS = {
    white: '#fff',
    black: '#000',
    blue: '#5D5FEE',
    grey: '#d4d4d4',
    light: '#ecf0f1',
    darkBlue: '#7978B5',
    red: 'red',
    green: '#3DBE29',
    lightgreen: '#cce6c8',
};