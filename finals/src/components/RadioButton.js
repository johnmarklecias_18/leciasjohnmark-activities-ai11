import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import COLORS from '../constants/colors.js';

export default function RadioButton({label, selected, onPress = () => {}}) {
    return (
        <TouchableOpacity
            onPress={() => {onPress()}}
            activeOpacity={0.7}
            style={{
                marginVertical: 10,
                marginHorizontal: 10,
                flexDirection: "row",
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Icon
                name={(selected)? "radiobox-marked" : "radiobox-blank"} 
                size={20} 
                style={{color: (selected) ? COLORS.green : COLORS.black }}
            />
            <Text style={{color: COLORS.black, fontSize: 15, marginLeft: 5}}>
            {label}
            </Text>
        </TouchableOpacity>
    );
};