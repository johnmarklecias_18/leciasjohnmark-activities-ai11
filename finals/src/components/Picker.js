import * as React from 'react';
import { StyleSheet, Text, View, Dimensions, Modal, FlatList, TouchableOpacity} from 'react-native';

const {width, height} = Dimensions.get('window');

export default function Picker({
    visible = false,
    items,
    label,
    onPress = () => {},
    onPress = () => {},
}) {
    
    return (
    <Modal visible={visible} animationType="slide" transparent={true}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.title} >{label}</Text>

            <FlatList
              data={items}
              renderItem={({item, index}) => {
                <TouchableOpacity onPress={() => onPickerPress(item.name)} key={index}>
                  <Text style={styles.subtitle}>{item.name}</Text>
                </TouchableOpacity>
              }}
            />

            <TouchableOpacity onPress={() => setIsModalVisible(false)}>
              <Text style={styles.subtitle}>Back</Text>
            </TouchableOpacity>
          </View>
        </View>
    </Modal>
    )
  };

  const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginTop: 10,
        marginHorizontal: 20
    },
    subtitle: {
        fontSize: 18,
        color: 'black',
        marginTop: 10,
        marginHorizontal: 20
    },
  });