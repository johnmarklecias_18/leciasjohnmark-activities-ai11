import * as React from 'react';
import { StyleSheet, Text, View, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export default function Item ({item}) {
    return (
      <View style={styles.container}>
        <View style={{paddingVertical: 7, paddingHorizontal: 15, maxWidth: width*0.65}}>
          <Text style={styles.title} numberOfLines={1}>{item.name}</Text>
          <Text style={styles.subtitle} numberOfLines={1}>{item.category}</Text>
          <Text style={styles.subtitle} numberOfLines={1}>{item.brand}</Text>
        </View>
        <View style={{paddingVertical: 7, paddingHorizontal: 15, alignItems: "flex-end"}}>
          <Text style={[styles.title, {fontWeight: "bold"}]}>{parseInt(item.stock).toLocaleString()}</Text>
          <Text style={styles.subtitle}>PHP  {parseInt(item.price).toLocaleString()}</Text>
        </View>
      </View>
    )
  };
  
  const styles = StyleSheet.create({
    container: {
        height: 80,
        backgroundColor: COLORS.white, 
        marginBottom: 5,
        marginHorizontal: 10,
        borderRadius: 15,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "flex-start",
        borderWidth: 0.5,
        borderColor: COLORS.grey,
    },
    title: {
        color: COLORS.black,
        fontSize: 18,
        fontWeight: "500"
    },
    subtitle: {
        color: COLORS.black,
        fontSize: 13,
    },
  });