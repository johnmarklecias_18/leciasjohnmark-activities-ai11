import * as React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, TextInput} from 'react-native';

const {width, height} = Dimensions.get('window');

export default function UpdateStockItem({
    item,
    selected,
    onSelect,
    onFocus = () => {},
    ...props
}) {
    const [isSelected, setIsSelected] = React.useState(selected);
    const [isFocused, setIsFocused] = React.useState(false);
    const [value, setValue] = React.useState("");
    const textInputRef = React.useRef();

    let newSelectedItem = {
        date: Date.now(),
        name: item.name,
        sku: item.sku,
        stockFlow: value,
        prevStockValue: item.stock,
    };

    return (
      <TouchableOpacity 
        onPress={() => {
            setIsSelected(true);
            setIsFocused(true);
            textInputRef.current?.focus()
        }}
        activeOpacity={1} 
        style={[styles.container, (isSelected) && {borderWidth: 3, borderColor: COLORS.green,}]}
    >
        <View style={{paddingVertical: 7, paddingHorizontal: 15, maxWidth: width*0.65}}>
          <Text style={styles.title} numberOfLines={1}>{item.name}</Text>
          <Text style={styles.subtitle}>{parseInt(item.stock).toLocaleString()}</Text>
        </View>
        <View style={{marginRight: 15, marginVertical: 10, alignItems: "flex-end"}}>
            <View style={styles.inputContainer}> 
              <TextInput
                value={(!selected)? value : (isFocused)? value : selected.stockFlow}
                onChangeText={(text) => {
                    setValue(text);
                }}
                keyboardType='numeric'
                ref={textInputRef}
                autoCorrect={false}
                onFocus={() => {onFocus(); setIsFocused(true); setIsSelected(true)}}
                onBlur={() => {
                    if(!value) setIsSelected(false)
                    else onSelect(newSelectedItem);
                }}
                placeholderTextColor={COLORS.grey}
                color={COLORS.black}
                {...props}
              />
            </View>
        </View>
      </TouchableOpacity>
    )
  };

  const styles = StyleSheet.create({
    container: {
      height: 60,
      backgroundColor: COLORS.white, 
      marginBottom: 5,
      marginHorizontal: 10,
      borderRadius: 15,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      borderWidth: 0.5,
      borderColor: COLORS.grey,
    },
    inputContainer: {
        height: 40,
        width: width*0.15,
        backgroundColor: COLORS.light,
        flexDirection: 'row',
        justifyContent: "center",
        borderWidth: 0.5,
        borderColor: COLORS.grey,
    },
    title: {
      color: COLORS.black,
      fontSize: 18,
      fontWeight: "500"
    },
    subtitle: {
      color: COLORS.black,
      fontSize: 13,
    },
  });