import React from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import COLORS from '../constants/colors';

export default function FloatingButton({ iconName, size =25, color=COLORS.white, style, onPress }) {
  return (
    <Icon
      name={iconName}
      size={size}
      color={color}
      style={[styles.icon, { ...style }]}
      onPress={onPress}
    />
  );
};

const styles = StyleSheet.create({
  icon: {
    backgroundColor: COLORS.green,
    padding: 15,
    borderRadius: 50,
    elevation: 5,
  },
});