import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// Constants
import COLORS from '../constants/colors';

export default function SearchBar({
    value, 
    onClear = () => {},
    containerStyle,
    ...props
}) {
  return (
    <View style={[styles.container, { ...containerStyle }]}>
      <TextInput
        value={value}
        style={styles.searchBar}
        placeholder='Search items here..'
        placeholderTextColor={COLORS.grey}
        color={COLORS.black}
        {...props}
      />
      {value ? (
        <Icon
          name='close'
          size={20}
          color={COLORS.grey}
          onPress={() => onClear()}
          style={{position: "absolute", right: 10}}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  searchBar: {
    borderWidth: 0.5,
    borderColor: COLORS.grey,
    borderRadius: 15,
    paddingLeft: 15,
    width: "100%"
  },
  container: {
    flexDirection: "row",
    alignItems: 'center',
    height: 40,
    borderRadius: 15,
    backgroundColor: COLORS.white,
  },
});