import * as React from 'react';
import {View, Text, TextInput, StyleSheet, TouchableWithoutFeedback, TouchableOpacity, Modal, FlatList, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import COLORS from '../constants/colors';

const {height} = Dimensions.get('window');

export default function Input ({
  width,
  label,
  iconName = null,
  error,
  items = null,
  onPickerPress,
  onFocus = () => {},
  ...props
}) {
  const [isFocused, setIsFocused] = React.useState(false);
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const textInputRef = React.useRef();

  return (
    <>
      <TouchableWithoutFeedback onPress = {() =>  textInputRef.current?.focus()}>
        <View style={{marginBottom: 10}}>
          <Text style={styles.label}>{label}</Text>
            <View
              style={[
                styles.inputContainer,
                {
                  borderColor: error
                    ? COLORS.red
                    : isFocused
                    ? COLORS.darkBlue
                    : "transparent",
                  alignItems: 'center',
                  width: width
                },
              ]}
            > 
              <TextInput
                ref={textInputRef}
                autoCorrect={false}
                onFocus={() => {
                  onFocus();
                  setIsFocused(true);
                }}
                onBlur={() => setIsFocused(false)}
                placeholderTextColor={COLORS.grey}
                color={COLORS.black}
                {...props}
              />
              <Icon 
                name={iconName} style={{color: COLORS.black, fontSize: 25}}
                onPress={() => setIsModalVisible(true)}
              />
            </View>
            {error && (
              <Text style={{color: COLORS.red, fontSize: 10, textAlign: "right"}}>
                {error}
              </Text>
            )}
        </View>
      </TouchableWithoutFeedback>
      <Modal visible={isModalVisible} animationType="slide" transparent={true}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.title} >Select {label}</Text>
              
            <FlatList
              data={items}
              renderItem={({item, index}) => {
                return (
                  <TouchableOpacity 
                    onPress={() => {
                      setIsModalVisible(false);
                      textInputRef.current?.focus();
                      onPickerPress(item.name);
                    }} 
                    key={index}
                  >
                    <Text style={styles.subtitle}>{item.name}</Text>
                  </TouchableOpacity>
                )
              }}
            />

            <TouchableOpacity onPress={() => {setIsModalVisible(false)}}>
              <Text style={[styles.subtitle, {fontWeight: "bold"}]}>Back</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    width: "90%",
    maxHeight: height*0.60,
    backgroundColor: COLORS.white,
    borderRadius: 20,
    alignItems: "center",
    elevation: 25
  },
  label: {
    marginVertical: 5,
    fontSize: 14,
    color: COLORS.black,
  },
  inputContainer: {
    height: 40,
    backgroundColor: COLORS.light,
    flexDirection: 'row',
    justifyContent: "space-between",
    paddingHorizontal: 15,
    borderWidth: 0.5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: COLORS.black,
    margin: 20,
    textAlign: 'center',
  },
  subtitle: {
      fontSize: 15,
      color: COLORS.black,
      margin: 20,
      textAlign: 'center',
  },
  listText: {
    fontSize: 15,
    color: COLORS.black,
    margin: 10,
    textAlign: 'center',
  },
});