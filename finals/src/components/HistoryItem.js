import * as React from 'react';
import { StyleSheet, Text, View, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export default function HistoryItem({item}) {
    let title = "";
    let stockFlow = 0;

    if(item.names) {
      if(item.names.length > 1) {
        title = item.names[0] + " and " + (item.names - 1) + " others";
      }
      else {
        title = item.names[0];
      }
    }

    if(item.stockFlow) {
      stockFlow = item.stockFlow.reduce((total, currentValue) => total = total + currentValue,0);
    }

    return (
      <View style={styles.container}>
        <View style={{paddingVertical: 7, paddingHorizontal: 15, maxWidth: width*0.65}}>
          <Text style={styles.title} numberOfLines={1}>{title}</Text>
          <Text style={styles.subtitle}>{new Date(item.date).toDateString()}</Text>
          <Text style={styles.subtitle}>{new Date(item.date).toTimeString().slice(0,8)}</Text>
        </View>
        <View style={{paddingVertical: 7, paddingHorizontal: 15, alignItems: "flex-end"}}>
          <Text style={[styles.title, {fontWeight: "bold"}]}>{stockFlow.toLocaleString()}</Text>
        </View>
        
      </View>
    )
  };

  const styles = StyleSheet.create({
    container: {
      height: 80,
      backgroundColor: COLORS.white, 
      marginBottom: 5,
      marginHorizontal: 10,
      borderRadius: 15,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "flex-start",
      borderWidth: 0.5,
      borderColor: COLORS.grey,
    },
    title: {
      color: COLORS.black,
      fontSize: 18,
      fontWeight: "500"
    },
    subtitle: {
      color: COLORS.black,
      fontSize: 13,
    },
  });