import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

const HistoryContext = createContext();

export default function HistoryProvider({ children }) {
  const [history, setHistory] = useState([]);

  const getHistory = async () => {
    try {
      const result = await AsyncStorage.getItem('@history');
      if (result !== null) {
        setHistory(JSON.parse(result));
        console.log("history retrieved" + result)
      }
    } catch (error) {
        console.log("no history")
    } 
  }

  useEffect(() => {
    getHistory();
  }, []);

  return (
    <HistoryContext.Provider value={{history, setHistory, getHistory }}>
      {children}
    </HistoryContext.Provider>
  );
};

export const useHistory = () => useContext(HistoryContext);