import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

const ItemsFilterContext = createContext();

export default function ItemsFilterProvider({ children }) {
    const [filter, setFilter] = useState({
        ascending: true,
        orderType: 'name'
    });

    const getItemsFilter = async () => {
        try {
        const result = await AsyncStorage.getItem('@itemsfilter');
        if (result !== null) {
            setFilter(JSON.parse(result));
            console.log("items filter retrieved" + result)
        }
        } catch (error) {
            console.log("no items filter")
        } 
    };

    const filterItems = (items) => {
        switch(filter.orderType) {
          case "name":
            return items.sort((a, b) => {
              if (filter.ascending) return (a.name > b.name) ? 1 : -1
              else return (a.name > b.name) ? -1 : 1 });
          case "category":
            return items.sort((a, b) => {
              if (filter.ascending) return (a.category > b.category) ? 1 : -1
              else return (a.category > b.category) ? -1 : 1 });
          case "brand":
            return items.sort((a, b) => {
              if (filter.ascending) return (a.brand > b.brand) ? 1 : -1
              else return (a.brand > b.brand) ? -1 : 1 });
        };
    };

    useEffect(() => {
        getItemsFilter();
    }, []);

    return (
        <ItemsFilterContext.Provider value={{filter, setFilter, getItemsFilter, filterItems }}>
        {children}
        </ItemsFilterContext.Provider>
    );
};

export const useItemsFilter = () => useContext(ItemsFilterContext);