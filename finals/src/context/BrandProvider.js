import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

const BrandContext = createContext();

export default function BrandProvider({ children }) {
  const [brands, setBrands] = useState([]);

  const getBrands = async () => {
    try {
      const result = await AsyncStorage.getItem('@brands');
      if (result !== null) {
        setBrands(JSON.parse(result));
        console.log("brands retrieved" + result)
      }
    } catch (error) {
        console.log("no brands")
    } 
  }

  useEffect(() => {
    getBrands();
  }, []);

  return (
    <BrandContext.Provider value={{brands, setBrands, getBrands }}>
      {children}
    </BrandContext.Provider>
  );
};

export const useBrands = () => useContext(BrandContext);