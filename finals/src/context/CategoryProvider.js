import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

const CategoryContext = createContext();

export default function CategoryProvider({ children }) {
  const [categories, setCategories] = useState([]);

  const getCategories = async () => {
    try {
      const result = await AsyncStorage.getItem('@categories');
      if (result !== null) {
        setCategories(JSON.parse(result));
        console.log("categories retrieved" + result)
      }
    } catch (error) {
        console.log("no categories")
    } 
  }

  useEffect(() => {
    getCategories();
  }, []);

  return (
    <CategoryContext.Provider value={{categories, setCategories, getCategories }}>
      {children}
    </CategoryContext.Provider>
  );
};

export const useCategories = () => useContext(CategoryContext);