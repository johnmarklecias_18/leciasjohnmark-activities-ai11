import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

const ItemHistoryContext = createContext();

export default function ItemHistoryProvider({ children }) {
  const [itemHistory, setItemHistory] = useState([]);

  const getItemHistory = async () => {
    try {
      const result = await AsyncStorage.getItem('@itemhistory');
      if (result !== null) {
        setItemHistory(JSON.parse(result));
        console.log("item history retrieved" + result)
      }
    } catch (error) {
        console.log("no item history")
    } 
  }

  useEffect(() => {
    getItemHistory();
  }, []);

  return (
    <ItemHistoryContext.Provider value={{itemHistory, setItemHistory, getItemHistory }}>
      {children}
    </ItemHistoryContext.Provider>
  );
};

export const useItemHistory = () => useContext(ItemHistoryContext);