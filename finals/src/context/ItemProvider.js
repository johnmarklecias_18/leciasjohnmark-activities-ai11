import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

const ItemContext = createContext();

export default function ItemProvider({ children }) {
  const [items, setItems] = useState([]);

  const getItems = async () => {
    try {
      const result = await AsyncStorage.getItem('@items');
      if (result !== null) {
        setItems(JSON.parse(result));
        console.log("found items" + result)
      }
    } catch (error) {
        console.log("items not found")
    } 
  }

  useEffect(() => {
    getItems();
  }, []);

  return (
    <ItemContext.Provider value={{items,  setItems, getItems }}>
      {children}
    </ItemContext.Provider>
  );
};

export const useItems = () => useContext(ItemContext);