import { StyleSheet, View, TextInput, SafeAreaView, Button, Alert} from 'react-native';
import React from "react";

export default function App() {

  const [text, onTextChange] = React.useState("");

  const displayTextOnAlert  = ()=>
    {
      if(text == "") {
        Alert.alert(
          "Empty",
          "Please enter text"
        )
      } else {
        Alert.alert(
          "You just typed:",
          "< "+text+" >"
        )
      }
    }
  

  return (
    <SafeAreaView style={styles.container}>
      <TextInput
        style={styles.textInput}
        onChangeText={onTextChange}
        value={text}
        placeholder={"Input Text Here"}
        selectionColor = "#4bdb90"
        
      />
      <View style={[{alignItems:'flex-end', marginTop: 16}]}>
        <Button
        title = {"Display Text"} 
        onPress = {displayTextOnAlert}
        color = '#4bdb90'
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    padding: 20
  },
  textInput: {
    height: 50,
    borderWidth:1,
    padding: 10,
    borderColor:'#4bdb90'
  }
});
